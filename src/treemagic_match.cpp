#include "treemagic_match.hpp"

#include "directory.hpp"

#include <compare>

namespace stream9::xdg::mime {

bool treemagic_match::
operator==(treemagic_match const& other) const noexcept
{
    return mime_type() == other.mime_type();
}

std::strong_ordering treemagic_match::
operator<=>(treemagic_match const& other) const noexcept
{
    if (auto cmp = priority() <=> other.priority(); cmp != 0) {
        return cmp;
    }
    else if (auto cmp = mime_type() <=> other.mime_type(); cmp != 0) {
        return cmp;
    }
    else {
        return directory() <=> other.directory();
    }
}

} // namespace stream9::xdg::mime
