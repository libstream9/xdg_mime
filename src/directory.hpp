#ifndef STREAM9_XDG_MIME_DIRECTORY_HPP
#define STREAM9_XDG_MIME_DIRECTORY_HPP

#include <stream9/xdg/mime/file_data.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include <compare>
#include <cstdint>

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/node.hpp>
#include <stream9/optional.hpp>

namespace stream9::xdg::mime {

class aliases;
class cache;
class database;
class glob_match;
class icons;
class magic_match;
class subclasses;
class treemagic_match;
class treemagics;

class directory
{
public:
    explicit directory(cstring_ptr const& path);
    ~directory();

    directory(directory const&) = delete;
    directory& operator=(directory const&) = delete;

    directory(directory&&) noexcept;
    directory& operator=(directory&&) noexcept;

    // accessor
    cstring_view path() const noexcept;

    class cache const& cache() const noexcept;
    class cache& cache() noexcept;

    // query
    bool has_entry(string_view mime_type) const;

    class aliases aliases() const noexcept;

    void glob_search(array<glob_match>&, string_view filename) const;
    void magic_search(array<magic_match>& result, file_data const&) const;

    std::uint32_t max_extent() const noexcept;

    class icons icons() const noexcept;
    class icons generic_icons() const noexcept;

    class subclasses subclasses() const noexcept;

    char const* xml_namespaces_search(string_view namespace_uri,
                                      string_view local_name) const noexcept;

    void treemagic_search(array<treemagic_match>& result,
                          database const&,
                          cstring_ptr const& root_path) const;

    bool has_glob_deleteall(string_view mime_type) const noexcept;
    bool has_magic_deleteall(string_view mime_type) const noexcept;

    string xml_entry(string_view mime_type) const;

    // comparison
    std::strong_ordering operator<=>(directory const&) const noexcept;
    bool operator==(directory const&) const noexcept;

private:
    string m_path;
    node<class cache> m_cache;
    mutable opt<node<treemagics>> m_treemagics;
};

} // namespace stream9::xdg::mime

#include "directory.tcc"

#endif // STREAM9_XDG_MIME_DIRECTORY_HPP
