#ifndef STREAM9_XDG_MIME_TREEMAGIC_TCC
#define STREAM9_XDG_MIME_TREEMAGIC_TCC

#include "treemagic.hpp"

namespace stream9::xdg::mime {

/*
 * treematch
 */
inline treematch::
treematch(string_view path,
          enum type t,
          bit_flags<enum option> o,
          opt<string_view> mime_type)
    try : m_path { path }
        , m_type { t }
        , m_options { o }
{
    if (mime_type) {
        m_mime_type = *mime_type;
    }
}
catch (...) {
    rethrow_error();
}

inline treematch::
treematch(string_view path,
          enum type t,
          bit_flags<enum option> o,
          opt<string_view> mime_type,
          array<treematch> children)
    try : m_path { path }
        , m_type { t }
        , m_options { o }
        , m_children { std::move(children) }
{
    if (mime_type) {
        m_mime_type = *mime_type;
    }
}
catch (...) {
    rethrow_error();
}

inline cstring_view treematch::path() const noexcept { return m_path; }
inline enum treematch::type treematch::type() const noexcept { return m_type; }
inline bool treematch::match_case() const noexcept { return m_options & option::match_case; }
inline bool treematch::executable() const noexcept { return m_options & option::executable; }
inline bool treematch::non_empty() const noexcept { return m_options & option::non_empty; }
inline cstring_view treematch::mime_type() const noexcept { return m_mime_type; }

/*
 * treemagic
 */
inline treemagic::
treemagic(string_view mime_type,
          int priority,
          array<treematch> matches)
    : m_mime_type { mime_type }
    , m_priority { priority }
    , m_matches { std::move(matches) }
{}

inline string const& treemagic::
mime_type() const noexcept
{
    return m_mime_type;
}

inline treemagic_priority treemagic::
priority() const noexcept
{
    return m_priority;
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_TREEMAGIC_TCC
