#ifndef STREAM9_XDG_MIME_XML_NAMESPACES_HPP
#define STREAM9_XDG_MIME_XML_NAMESPACES_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "endian.hpp"

#include <cstdint>
#include <compare>
#include <iterator>
#include <ranges>

#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>
#include <stream9/ranges/range_facade.hpp>

namespace stream9::xdg::mime {

class xml_namespace_iterator;

struct namespace_list;
struct namespace_entry;

class xml_namespace
{
public:
    xml_namespace(char const* base, namespace_entry const*) noexcept;

    cstring_view namespace_uri() const noexcept;
    cstring_view local_name() const noexcept;
    cstring_view mime_type() const noexcept;

    template<std::size_t I>
        requires (I < 3)
    decltype(auto) get() const noexcept;

private:
    char const* m_base; // non-null
    namespace_entry const* m_entry; // non-null
};

class xml_namespaces : public stream9::ranges::range_facade<xml_namespaces>
{
public:
    using const_iterator = xml_namespace_iterator;

public:
    xml_namespaces(char const* base, card32_t offset) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    opt<cstring_view>
        search(string_view namespace_uri,
               string_view local_name) const noexcept;

private:
    char const* m_base; // non-null
    namespace_list const* m_record; // non-null
};

/**
 * @model std::random_access_iterator
 */
class xml_namespace_iterator : public iter::iterator_facade<
                                            xml_namespace_iterator,
                                            std::random_access_iterator_tag,
                                            xml_namespace,
                                            xml_namespace >
{
public:
    xml_namespace_iterator() = default;
    xml_namespace_iterator(char const* base, namespace_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    xml_namespace dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(xml_namespace_iterator const&) const noexcept;

    bool equal(xml_namespace_iterator const&) const noexcept;

    std::strong_ordering compare(xml_namespace_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    namespace_entry const* m_entry = nullptr;
};

} // namespace stream9::xdg::mime

#include "xml_namespaces.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<
                                stream9::xdg::mime::xml_namespaces> = true;

} // namespace std::ranges

namespace std {

template<>
struct tuple_size<stream9::xdg::mime::xml_namespace>
    : integral_constant<size_t, 3> {};

template<size_t I>
    requires (I < 3)
struct tuple_element<I, stream9::xdg::mime::xml_namespace>
{
    using type = char const*;
};

} // namespace std

#endif // STREAM9_XDG_MIME_XML_NAMESPACES_HPP
