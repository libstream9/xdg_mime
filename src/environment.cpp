#include <stream9/xdg/mime/environment.hpp>

#include "real_path_icase.hpp"

#include <cstdlib>

#include <stream9/xdg/mime/error.hpp>

#include <stream9/filesystem/load_file.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/fnmatch.hpp>
#include <stream9/linux/realpath.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/path/normalize.hpp>
#include <stream9/strings/equal.hpp>

namespace stream9::xdg::mime {

environment&
env() /*noexcept*/
{
    static environment instance {};

    return instance;
}

environment::file_type_t environment::
file_type(cstring_ptr const& pathname)
{
    try {
        auto o_st = lx::nothrow::stat(pathname);
        if (o_st) {
            using enum file_type_t;

            auto m = o_st->st_mode;
            if (S_ISREG(m)) {
                return regular;
            }
            else if (S_ISDIR(m)) {
                return directory;
            }
            else if (S_ISSOCK(m)) {
                return socket;
            }
            else if (S_ISFIFO(m)) {
                return fifo;
            }
            else if (S_ISCHR(m)) {
                return character_device;
            }
            else if (S_ISBLK(m)) {
                return block_device;
            }
            else {
                throw_error(errc::unknown_file_type, {
                    { "stat", *o_st }
                });
            }
        }
        else {
            throw_error("stat(2)", o_st.error());
        }
    }
    catch (...) {
        throw_error(errc::fail_to_get_file_status, {
            { "pathname", pathname }
        });
    }
}

bool environment::
is_symbolic_link(cstring_ptr const& pathname) /*noexcept*/
{
    auto o_st = lx::nothrow::lstat(pathname);
    if (o_st) {
        return S_ISLNK(o_st->st_mode);
    }
    else {
        return false;
    }
}

bool environment::
is_directory(cstring_ptr const& pathname) /*noexcept*/
{
    try {
        return file_type(pathname) == file_type_t::directory;
    }
    catch (...) {
        return false;
    }
}

bool environment::
is_regular_file(cstring_ptr const& pathname) /*noexcept*/
{
    try {
        return file_type(pathname) == file_type_t::regular;
    }
    catch (...) {
        return false;
    }
}

bool environment::
is_file_exists(cstring_ptr const& pathname) /*noexcept*/
{
    return static_cast<bool>(lx::nothrow::stat(pathname));
}

bool environment::
is_executable(cstring_ptr const& pathname) /*noexcept*/
{
    auto o_st = lx::nothrow::stat(pathname);
    if (o_st) {
        auto m = o_st->st_mode;
        return (m & S_IXUSR) || (m & S_IXGRP) || (m & S_IXOTH);
    }
    else {
        return false;
    }
}

bool environment::
is_non_empty_directory(cstring_ptr const& pathname) /*noexcept*/
{
    try {
        lx::directory dir { pathname };
        return dir.begin() != dir.end();
    }
    catch (...) {
        return false;
    }
}

bool environment::
is_mount_point(cstring_ptr const& dir_path)
{
    try {
        string cmdline = "mountpoint --quiet ";
        cmdline.append(dir_path);

        auto rv = std::system(cmdline.c_str());
        if (rv == -1) {
            throw_error(
                "system(3)",
                lx::make_error_code(errno)
            );
        }

        return rv == 0;
    }
    catch (...) {
        rethrow_error();
    }
}

string environment::
normalize_path(cstring_ptr const& path)
{
    try {
        return path::normalize(path);
    }
    catch (...) {
        rethrow_error({
            { "path", path }
        });
    }
}

string environment::
real_path(cstring_ptr const& path)
{
    try {
        return lx::realpath(path);
    }
    catch (...) {
        throw_error(errc::fail_to_get_canonical_path, {
            { "path", path }
        });
    }
}

opt<string> environment::
real_path_icase(cstring_ptr const& pathname)
{
    try {
        return mime::real_path_icase(pathname);
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname }
        });
    }
}

string environment::
load_file(cstring_ptr const& pathname)
{
    try {
        return fs::load_string(pathname);
    }
    catch (...) {
        throw_error(errc::fail_to_load_file, {
            { "pathname", pathname }
        });
    }
}

void environment::
load_file(cstring_ptr const& pathname, array<char>& buffer)
{
    try {
        fs::load_file(pathname, buffer);
    }
    catch (...) {
        throw_error(errc::fail_to_load_file, {
            { "pathname", pathname }
        });
    }
}

bool environment::
match_filename(cstring_ptr const& pattern,
               cstring_ptr const& filename,
               bool case_insensitive/* = true*/)
{
    auto flags = case_insensitive ? 0 : FNM_CASEFOLD;
    return lx::fnmatch(pattern, filename, flags);
}

} // namespace stream9::xdg::mime
