#include "rapidxml_util.hpp"

#include <stdexcept>

namespace stream9::xdg::mime {

cstring_view
get_attribute(rapidxml::xml_node<> const& node, string_view name)
{
    try {
        auto* const attr = first_attribute(node, name);
        if (attr) {
            return value(*attr);
        }
        else {
            return "";
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void
set_attribute(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& node,
    string_view name, string_view value)
{
    try {
        assert(!name.empty());

        if (auto* attr = first_attribute(node, name); attr) {
            if (value.empty()) {
                node.remove_attribute(attr);
            }
            else {
                attr->value(value.data(), value.size());
            }
        }
        else {
            auto& new_attr = create_attribute(mp, name, value);
            node.append_attribute(&new_attr);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view
allocate_string(rapidxml::memory_pool<>& mp, string_view s)
{
    try {
        if (s.empty()) return "";

        auto* const ptr =
            mp.allocate_string(s.data(), s.size() + 1); // +1 for null
        ptr[s.size()] = '\0';

        return { ptr, s.size() };
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<>&
create_element(rapidxml::memory_pool<>& mp,
    string_view name, string_view value)
{
    try {
        return *mp.allocate_node(
            rapidxml::node_element,
            name.data(),
            value.data(),
            name.size(),
            value.size() );
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_attribute<>&
create_attribute(rapidxml::memory_pool<>& mp,
    string_view name, string_view value)
{
    try {
        return *mp.allocate_attribute(
                name.data(), value.data(), name.size(), value.size() );
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<>*
first_element(rapidxml::xml_node<> const& parent, string_view name)
{
    try {
        auto* node = parent.first_node(name.data(), name.size());
        while (node) {
            if (node->type() == rapidxml::node_element) {
                return node;
            }

            node = next_sibling(*node, name);
        }

        return nullptr;
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<>*
first_element(rapidxml::xml_node<>& parent,
              string_view name,
              string_view attr_name,
              string_view attr_value)
{
    try {
        for (auto& node: element_iterator { parent, name }) {
            auto value = get_attribute(node, attr_name);
            if (value == attr_value) {
                return &node;
            }
        }

        return nullptr;
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view
get_text(rapidxml::xml_node<>& parent,
         string_view element,
         string_view language)
{
    try {
        for (auto& node: element_iterator { parent, element }) {
            auto lang = get_attribute(node, "xml:lang");
            if (lang == language) {
                return value(node);
            }
        }

        return "";
    }
    catch (...) {
        rethrow_error();
    }
}

void
set_text(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& parent,
         string_view element, string_view language,
         string_view text)
{
    try {
        for (auto& node: element_iterator { parent, element }) {
            auto lang = get_attribute(node, "xml:lang");
            if (lang == language) {
                set_value(node, text);
                return;
            }
        }

        auto& el = create_element(mp, element, text);
        set_attribute(mp, el, "xml:lang", language);
        append_child(parent, el);
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<>&
root_element(rapidxml::xml_document<>& doc)
{
    try {
        auto* node = doc.first_node();
        while (node) {
            if (node->type() == rapidxml::node_element) {
                return *node;
            }

            node = next_sibling(*node);
        }

        throw_error(errc::no_root_element);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
