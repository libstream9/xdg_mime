#ifndef STREAM9_XDG_MIME_GLOB_MATCH_TCC
#define STREAM9_XDG_MIME_GLOB_MATCH_TCC

#include "glob_match.hpp"

#include <stream9/xdg/mime/error.hpp>

namespace stream9::xdg::mime {

inline glob_match::
glob_match(string pattern,
           cstring_view mime_type,
           glob_flags flags,
           glob_weight weight,
           class directory const& dir)
    try : m_pattern { std::move(pattern) }
        , m_mime_type { mime_type }
        , m_flags { flags }
        , m_weight { weight }
        , m_dir { &dir }
{}
catch (...) {
    rethrow_error();
}

inline cstring_view glob_match::
mime_type() const noexcept
{
    return m_mime_type;
}

inline cstring_view glob_match::
pattern() const noexcept
{
    return m_pattern;
}

inline glob_flags glob_match::
flags() const noexcept
{
    return m_flags;
}

inline glob_weight glob_match::
weight() const noexcept
{
    return m_weight;
}

inline class directory const& glob_match::
directory() const noexcept
{
    return *m_dir;
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_GLOB_MATCH_TCC
