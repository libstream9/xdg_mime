#ifndef STREAM9_XDG_MIME_TREEMAGIC_MATCH_HPP
#define STREAM9_XDG_MIME_TREEMAGIC_MATCH_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "treemagic.hpp"
#include "treemagic_priority.hpp"

#include <compare>

#include <stream9/cstring_view.hpp>

namespace stream9::xdg::mime {

class directory;

class treemagic_match
{
public:
    treemagic_match(class directory const&, treemagic const&) noexcept;

    class directory const& directory() const noexcept;

    cstring_view mime_type() const noexcept;
    treemagic_priority priority() const noexcept;

    bool operator==(treemagic_match const&) const noexcept;
    std::strong_ordering operator<=>(treemagic_match const&) const noexcept;

private:
    class directory const* m_directory; // non-null
    treemagic const* m_magic; // non-null
};

inline treemagic_match::
treemagic_match(class directory const& dir, treemagic const& magic) noexcept
    : m_directory { &dir }
    , m_magic { &magic }
{}

inline class directory const& treemagic_match::
directory() const noexcept
{
    return *m_directory;
}

inline cstring_view treemagic_match::
mime_type() const noexcept
{
    return m_magic->mime_type();
}

inline treemagic_priority treemagic_match::
priority() const noexcept
{
    return m_magic->priority();
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_TREEMAGIC_MATCH_HPP
