#include "cache.hpp"

#include <stream9/xdg/mime/error.hpp>

#include "aliases.hpp"
#include "directory.hpp"
#include "globs.hpp"
#include "icons.hpp"
#include "magic.hpp"
#include "reverse_suffix_tree.hpp"
#include "subclasses.hpp"
#include "xml_namespaces.hpp"

#include <cassert>

#include <stream9/array.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace stream9::xdg::mime {

struct header {
    card16_t major_version;
    card16_t minor_version;
    card32_t alias_list_offset;
    card32_t parent_list_offset;
    card32_t literal_list_offset;
    card32_t reverse_suffix_tree_offset;
    card32_t glob_list_offset;
    card32_t magic_list_offset;
    card32_t namespace_list_offset;
    card32_t icons_list_offset;
    card32_t generic_icons_list_offset;
};

class cache::impl
{
public:
    impl(string_view dir)
    {
        try {
            using path::operator/;
            m_path = dir / "mime.cache";

            env().load_file(m_path, m_data);
        }
        catch (...) {
            rethrow_error();
        }
    }

    cstring_view path() const noexcept { return m_path; }

    char const* base_addr() const noexcept { return m_data.data(); }

    struct header const& header() const noexcept
    {
        return *reinterpret_cast<struct header const*>(base_addr());
    }

    void reload()
    {
        try {
            env().load_file(m_path, m_data);
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    string m_path;
    array<char> m_data;
};

cache::
cache(string_view dir_path)
    try : m_p { dir_path }
{
    if (major_version() != 1 || minor_version() != 2) {
        throw_error(errc::cache_version_mismatch, {
            { "major", major_version() },
            { "minor", minor_version() },
        });
    }
}
catch (...) {
    rethrow_error();
}

cache::~cache() = default;

cstring_view cache::path() const noexcept { return m_p->path(); }
uint16_t cache::major_version() const noexcept { return m_p->header().major_version; }
uint16_t cache::minor_version() const noexcept { return m_p->header().minor_version; }

class aliases cache::
aliases() const noexcept
{
    return { m_p->base_addr(), m_p->header().alias_list_offset };
}

class subclasses cache::
subclasses() const noexcept
{
    return {
        m_p->base_addr(),
        m_p->header().parent_list_offset
    };
}

class globs cache::
globs() const noexcept
{
    return {
        *this,
        m_p->base_addr(),
        m_p->header().glob_list_offset
    };
}

class globs cache::
literals() const noexcept
{
    return {
        *this,
        m_p->base_addr(),
        m_p->header().literal_list_offset
    };
}

class suffix_tree cache::
reverse_suffix_tree() const noexcept
{
    return {
        *this,
        m_p->base_addr(),
        m_p->header().reverse_suffix_tree_offset
    };
}

class magics cache::
magics() const noexcept
{
    return { *this, m_p->base_addr(), m_p->header().magic_list_offset };
}

class xml_namespaces cache::
xml_namespaces() const noexcept
{
    return {
        m_p->base_addr(),
        m_p->header().namespace_list_offset
    };
}

class icons cache::
icons() const noexcept
{
    return {
        m_p->base_addr(),
        m_p->header().icons_list_offset
    };
}

class icons cache::
generic_icons() const noexcept
{
    return {
        m_p->base_addr(),
        m_p->header().generic_icons_list_offset
    };
}

void cache::
glob_search(array<glob_record>& result,
            string_view filename) const
{
    try {
        literals().literal_search(result, filename);

        reverse_suffix_tree().search(result, filename);

        globs().fnmatch_search(result, filename);
    }
    catch (...) {
        rethrow_error();
    }
}

void cache::
magic_search(array<magic>& result,
             file_data const& data) const
{
    try {
        magics().search(result, data);
    }
    catch (...) {
        rethrow_error();
    }
}

bool cache::
has_glob_deleteall(string_view mime_type) const noexcept
{
    return literals().has_match(mime_type, "__NOGLOBS__");
}

bool cache::
has_magic_deleteall(string_view mime_type) const noexcept
{
    string_view data = "__NOMAGIC__";
    return magics().has_match(mime_type, data);
}

void cache::
reload()
{
    try {
        m_p->reload();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
