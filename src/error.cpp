#include <stream9/xdg/mime/error.hpp>

#include <stream9/string.hpp>
#include <stream9/strings/stream.hpp>

namespace stream9::xdg::mime {

std::error_category const&
error_category() noexcept
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept { return "stream9::xdg::mime"; }

        std::string message(int ec) const
        {
            switch (static_cast<errc>(ec)) {
                using enum errc;
                case ok:
                    return "ok";
                case parse_error:
                    return "parse error";
                case fail_to_load_file:
                    return "fail to load file";
                case cache_version_mismatch:
                    return "cache version mismatch";
                case no_root_element:
                    return "no_root_element";
                case file_does_not_exist:
                    return "file does not exist";
                case directory_does_not_exist:
                    return "directory does not exist";
                case no_default_mime_type:
                    return "no default MIME-type";
                case unknown_file_type:
                    return "unknown file type";
                case fail_to_get_file_status:
                    return "fail to get file status";
                case fail_to_get_canonical_path:
                    return "fail to get canonical path";
                default:
                    return "unknown error";
            }
        }
    } instance;

    return instance;
}

} // namespace stream9::xdg::mime
