#ifndef STREAM9_XDG_MIME_DATABASE_IMPL_HPP
#define STREAM9_XDG_MIME_DATABASE_IMPL_HPP

#include <stream9/xdg/mime/database.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include "directory.hpp"

#include <cstdint>

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/optional.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime {

class glob_match;
class magic_match;
class mime_type;
class treemagic_match;

class database_impl
{
public:
    database_impl(database& db) noexcept : m_db { db } {}

    auto& dirs() noexcept { return m_dirs; }
    auto const& dirs() const noexcept { return m_dirs; }

    mime_type default_mime_type() const;
    mime_type default_mime_type(string_view name) const;
    mime_type default_mime_type_for_data(file_data const& data) const;

    std::uint32_t get_max_extent() const noexcept;

    array<glob_match> glob_search(string_view filename) const;

    array<magic_match> magic_search(file_data const& data) const;

    database::mime_type_set xml_search(string_view text) const;

    database::mime_type_set
        xml_search(string_view namespace_uri,
                   string_view local_name) const;

    mime_type select_match(array<glob_match>& glob_matches,
                           array<magic_match>& magic_matches,
                           file_data const& data);

    array<treemagic_match> filesystem_search(cstring_ptr const& root) const;

    opt<directory&> append_directory(cstring_ptr const& dir_path);

    void remove_directory(cstring_ptr const& dir_path);
    void remove_directory(directory const&);

private:
    database& m_db;
    array<directory> m_dirs;
};

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_DATABASE_IMPL_HPP
