#include "directory.hpp"

#include <stream9/xdg/mime/environment.hpp>
#include <stream9/xdg/mime/error.hpp>

#include "aliases.hpp"
#include "cache.hpp"
#include "icons.hpp"
#include "magic_match.hpp"
#include "glob_match.hpp"
#include "glob_record.hpp"
#include "subclasses.hpp"
#include "treemagic.hpp"
#include "xml_namespaces.hpp"

#include <stream9/emplace_back.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <algorithm>

namespace stream9::xdg::mime {

directory::
directory(cstring_ptr const& path)
    try : m_path { env().normalize_path(path) }
        , m_cache { m_path }
{
    if (!env().is_directory(path)) {
        throw_error(errc::directory_does_not_exist);
    }
}
catch (...) {
    rethrow_error({
        { "path", path }
    });
}

directory::~directory() = default;

directory::directory(directory&&) noexcept = default;
directory& directory::operator=(directory&&) noexcept = default;

bool directory::
has_entry(string_view mime_type) const
{
    using path::operator/;

    try {
        auto p = path() / mime_type;
        p += ".xml";

        return env().is_regular_file(p);
    }
    catch (...) {
        rethrow_error();
    }
}

class aliases directory::
aliases() const noexcept
{
    return m_cache->aliases();
}

void directory::
glob_search(array<glob_match>& result, string_view filename) const
{
    using std::ranges::remove_if;

    try {
        auto&& [it, end] = remove_if(result,
            [&](auto&& match) {
                return has_glob_deleteall(match.mime_type());
            });
        result.erase(it, end);

        array<glob_record> globs;
        m_cache->glob_search(globs, filename);

        for (auto& glob: globs) {
            emplace_back(result,
                std::move(glob.pattern),
                glob.mime_type,
                glob.flags,
                glob.weight,
                *this
            );
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void directory::
magic_search(array<magic_match>& result, file_data const& data) const
{
    using std::ranges::remove_if;

    try {
        auto&& [it, end] = remove_if(result,
            [&](auto&& match) {
                return has_magic_deleteall(match.mime_type());
            });
        result.erase(it, end);

        array<magic> magics;
        m_cache->magic_search(magics, data);

        for (auto const& magic: magics) {
            emplace_back(result, *this, magic);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

std::uint32_t directory::
max_extent() const noexcept
{
    return m_cache->magics().max_extent();
}

class icons directory::
icons() const noexcept
{
    return m_cache->icons();
}

class icons directory::
generic_icons() const noexcept
{
    return m_cache->generic_icons();
}

class subclasses directory::
subclasses() const noexcept
{
    return cache().subclasses();
}

char const* directory::
xml_namespaces_search(string_view namespace_uri,
                      string_view local_name) const noexcept
{
    auto const o_ns =
        m_cache->xml_namespaces().search(namespace_uri, local_name);
    if (o_ns) {
        return *o_ns;
    }
    else {
        return nullptr;
    }
}

void directory::
treemagic_search(array<treemagic_match>& result,
                 database const& db,
                 cstring_ptr const& root_path) const
{
    try {
        if (!m_treemagics) {
            m_treemagics.emplace(m_path);
        }

        array<treemagic const*> magics;
        m_treemagics->search(magics, db, root_path);

        for (auto const& magic: magics) {
            emplace_back(result, *this, *magic);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

bool directory::
has_glob_deleteall(string_view const mime_type) const noexcept
{
    return m_cache->has_glob_deleteall(mime_type);
}

bool directory::
has_magic_deleteall(string_view const mime_type) const noexcept
{
    return m_cache->has_magic_deleteall(mime_type);
}

string directory::
xml_entry(string_view const mime_type) const
{
    using path::operator/;

    try {
        auto path = m_path / mime_type;
        path += ".xml";
        if (env().is_regular_file(path)) {
            return path;
        }
        else {
            return {};
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
