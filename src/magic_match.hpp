#ifndef STREAM9_XDG_MIME_MAGIC_MATCH_HPP
#define STREAM9_XDG_MIME_MAGIC_MATCH_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "magic.hpp"
#include "magic_priority.hpp"

#include <compare>
#include <iosfwd>

#include <stream9/cstring_view.hpp>

namespace stream9::xdg::mime {

class directory;
class magic;

class magic_match
{
public:
    magic_match(class directory const&, class magic const&) noexcept;

    // accessor
    class directory const& directory() const noexcept;
    class magic const& magic() const noexcept;

    // query
    cstring_view mime_type() const noexcept;
    magic_priority priority() const noexcept;
    bool has_deleteall() const noexcept;

    // compare
    bool operator==(magic_match const&) const noexcept;
    std::partial_ordering operator<=>(magic_match const&) const noexcept;

private:
    class directory const* m_dir; // non-null
    class magic m_magic; // non-null
};

std::ostream& operator<<(std::ostream&, magic_match const&);

} // namespace stream9::xdg::mime

#include "magic_match.tcc"

#endif // STREAM9_XDG_MIME_MAGIC_MATCH_HPP
