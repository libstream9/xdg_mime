#ifndef STREAM9_XDG_MIME_ALIASES_HPP
#define STREAM9_XDG_MIME_ALIASES_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "endian.hpp"

#include <compare>
#include <iterator>
#include <ranges>
#include <tuple>

#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/optional.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

class alias_iterator;

struct alias_list;
struct alias_list_entry;

class alias_entry
{
public:
    alias_entry(char const* base, alias_list_entry const*) noexcept;

    cstring_view alias() const noexcept;
    cstring_view mime_type() const noexcept;

    template<std::size_t>
    decltype(auto) get() const noexcept;

private:
    char const* m_base; // non-null
    alias_list_entry const* m_entry; // non-null
};

class aliases : public stream9::ranges::range_facade<aliases>
{
public:
    using const_iterator = alias_iterator;

public:
    aliases(char const* base, card32_t offset) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    opt<cstring_view> search(string_view alias) const noexcept;

private:
    friend class alias_iterator;

    char const* m_base; // non-null
    alias_list const* m_record; // non-null
};

class alias_iterator : public iter::iterator_facade<alias_iterator,
                                                       std::random_access_iterator_tag,
                                                       alias_entry,
                                                       alias_entry >
{
public:
    alias_iterator() = default;
    alias_iterator(char const* base, alias_list_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    alias_entry dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(alias_iterator const&) const noexcept;

    bool equal(alias_iterator const&) const noexcept;

    std::strong_ordering compare(alias_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    alias_list_entry const* m_entry = nullptr;
};

} // namespace stream9::xdg::mime

#include "aliases.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<stream9::xdg::mime::aliases> = true;

} // namespace std::ranges

namespace std {

template<>
struct tuple_size<stream9::xdg::mime::alias_entry>
    : integral_constant<size_t, 2> {};

template<size_t I>
    requires (I < 2)
struct tuple_element<I, stream9::xdg::mime::alias_entry>
{
    using type = char const*;
};

} // namespace std

#endif // STREAM9_XDG_MIME_ALIASES_HPP
