#ifndef STREAM9_XDG_MIME_ICONS_HPP
#define STREAM9_XDG_MIME_ICONS_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "endian.hpp"

#include <compare>
#include <cstdint>
#include <iterator>
#include <ranges>

#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/optional.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

class icon_iterator;

struct icon_list;
struct icon_list_entry;

class icon
{
public:
    icon(char const* base, icon_list_entry const*) noexcept;

    cstring_view mime_type() const noexcept;
    cstring_view name() const noexcept;

    template<std::size_t I>
        requires (I < 2)
    decltype(auto) get() const noexcept;

private:
    char const* m_base; // non-null
    icon_list_entry const* m_entry; // non-null
};

class icons : public stream9::ranges::range_facade<icons>
{
public:
    using const_iterator = icon_iterator;

public:
    icons(char const* base, card32_t offset) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    opt<icon> search(string_view mime_type) const noexcept;

private:
    char const* m_base; // non-null
    icon_list const* m_record; // non-null
};

/**
 * @model std::random_access_iterator
 */
class icon_iterator : public iter::iterator_facade<
                                            icon_iterator,
                                            std::random_access_iterator_tag,
                                            icon,
                                            icon >
{
public:
    icon_iterator() = default;
    icon_iterator(char const* base, icon_list_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    icon dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(icon_iterator const&) const noexcept;

    bool equal(icon_iterator const&) const noexcept;

    std::strong_ordering compare(icon_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    icon_list_entry const* m_entry = nullptr;
};

} // namespace stream9::xdg::mime

#include "icons.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<
                                stream9::xdg::mime::icons> = true;

} // namespace std::ranges

namespace std {

template<>
struct tuple_size<stream9::xdg::mime::icon>
    : integral_constant<size_t, 2> {};

template<size_t I>
    requires (I < 2)
struct tuple_element<I, stream9::xdg::mime::icon>
{
    using type = char const*;
};

} // namespace std

#endif // STREAM9_XDG_MIME_ICONS_HPP
