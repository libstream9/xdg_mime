#ifndef STREAM9_XDG_MIME_TREEMAGIC_PRIORITY_HPP
#define STREAM9_XDG_MIME_TREEMAGIC_PRIORITY_HPP

#include <cstdint>
#include <stream9/safe_integer.hpp>

namespace stream9::xdg::mime {

using treemagic_priority = safe_integer<std::int32_t, 0, 100>;

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_TREEMAGIC_PRIORITY_HPP
