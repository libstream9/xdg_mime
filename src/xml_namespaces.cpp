#include "xml_namespaces.hpp"

#include <algorithm>

namespace stream9::xdg::mime {

opt<cstring_view> xml_namespaces::
search(string_view namespace_uri,
       string_view local_name) const noexcept
{
    opt<cstring_view> result;

    auto r = std::ranges::equal_range(*this, namespace_uri, {},
        [](auto&& ns) { return ns.namespace_uri(); } );

    auto it = std::ranges::find(r, local_name,
        [](auto&& ns) { return ns.local_name(); } );

    if (it != end()) {
        result.emplace((*it).mime_type());
    }

    return result;
}

} // namespace stream9::xdg::mime
