#include "magic_match.hpp"

#include "directory.hpp"
#include "magic.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <ostream>

namespace stream9::xdg::mime {

bool magic_match::
has_deleteall() const noexcept
{
    return m_magic.has_magic_deleteall();
}

std::partial_ordering magic_match::
operator<=>(magic_match const& o) const noexcept
{
    if (auto cmp = priority() <=> o.priority(); cmp != 0) {
        return cmp;
    }
    else {
        return *m_dir <=> *(o.m_dir);
    }
}

std::ostream&
operator<<(std::ostream& os, magic_match const& m)
{
    try {
        return os << m.magic();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
