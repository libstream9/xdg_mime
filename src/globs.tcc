#ifndef STREAM9_XDG_MIME_GLOBS_TCC
#define STREAM9_XDG_MIME_GLOBS_TCC

#include "globs.hpp"

namespace stream9::xdg::mime {

struct glob_entry {
    card32_t glob_offset;
    card32_t mime_type_offset;
    card24_t flags;
    card8_t weight;
};

struct glob_list {
    card32_t n_globs;
    glob_entry entries[1];
};

/*
 * glob
 */
inline glob::
glob(char const* const base, glob_entry const* const entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline cstring_view glob::
pattern() const noexcept
{
    return m_base + m_entry->glob_offset;
}

inline cstring_view glob::
mime_type() const noexcept
{
    return m_base + m_entry->mime_type_offset;
}

inline glob_flags glob::
flags() const noexcept
{
    std::uint32_t flags = m_entry->flags;
    return static_cast<glob_flag>(flags);
}

inline glob_weight glob::
weight() const noexcept
{
    return m_entry->weight.value();
}

template<std::size_t I>
    requires (I < 4)
decltype(auto) glob::
get() const noexcept
{
    if constexpr (I == 0) {
        return pattern();
    }
    else if constexpr (I == 1) {
        return mime_type();
    }
    else if constexpr (I == 2){
        return flags();
    }
    else {
        return weight();
    }
}

/*
 * globs
 */
inline globs::
globs(class cache const& c, char const* base, card32_t offset) noexcept
    : m_cache { &c }
    , m_base { base }
    , m_record { reinterpret_cast<glob_list const*>(m_base + offset) }
{}

inline class cache const& globs::
cache() const noexcept
{
    return *m_cache;
}

inline globs::const_iterator globs::
begin() const noexcept
{
    return { m_base, m_record->entries };
}

inline globs::const_iterator globs::
end() const noexcept
{
    return { m_base, m_record->entries + size() };
}

inline std::uint32_t globs::
size() const noexcept
{
    return m_record->n_globs;
}

/*
 * glob_iterator
 */
inline glob_iterator::
glob_iterator(char const* base, glob_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline glob glob_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void glob_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void glob_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void glob_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline glob_iterator::difference_type glob_iterator::
distance_to(glob_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool glob_iterator::
equal(glob_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering glob_iterator::
compare(glob_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_GLOBS_TCC
