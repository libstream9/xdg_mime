#include <stream9/xdg/mime/database_updater.hpp>

#include "cache.hpp"
#include "database_impl.hpp"
#include "directory.hpp"

#include <stream9/xdg/mime/environment.hpp>

#include <stream9/bits/predicate.hpp> // any_of
#include <stream9/json.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/log.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/projection.hpp>
#include <stream9/ranges/linear_find.hpp>
#include <stream9/source_location.hpp>
#include <stream9/strings/empty.hpp>
#include <stream9/unique_array.hpp>
#include <stream9/xdg/base_dir.hpp>

#include <utility>

namespace stream9::xdg::mime {

struct database_updater::impl
{
    enum class watch_type {
        data_dir,
        mime_dir,
    };

    struct watch {
        watch_type type;
        int wd;
        string path;
        opt<directory&> dir;
    };

    database_impl& m_db;
    lx::inotify m_inotify;
    unique_array<watch, less, project<&watch::wd>> m_watches;

    impl(database_impl& db)
        try : m_db { db }
            , m_inotify { IN_NONBLOCK }
    {
        add_data_dir(xdg::data_home());

        for (auto& dir: xdg::data_dirs()) {
            add_data_dir(dir);
        }
    }
    catch (...) {
        rethrow_error();
    }

    void
    add_data_dir(cstring_ptr dir_path)
    {
        using path::operator/;

        try {
            if (!env().is_directory(dir_path)) return;

            add_data_dir_watch(dir_path);

            auto mime_dir = dir_path / "mime";
            if (env().is_directory(mime_dir)) {
                add_mime_dir_watch(mime_dir);
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    add_data_dir_watch(cstring_ptr path)
    {
        try {
            std::uint32_t mask = IN_CREATE | IN_MOVED_TO
                      | IN_DELETE | IN_MOVED_FROM
                      | IN_DELETE_SELF | IN_MOVE_SELF;

            auto wd = m_inotify.add_watch(path, mask);

            m_watches.insert({
                watch_type::data_dir,
                wd,
                string(path),
                null
            });
        }
        catch (...) {
            rethrow_error();
        }
    }

    watch&
    add_mime_dir_watch(cstring_ptr path)
    {
        try {
            std::uint32_t mask = IN_CLOSE_WRITE | IN_MOVED_TO
                      | IN_DELETE | IN_MOVED_FROM
                      | IN_DELETE_SELF | IN_MOVE_SELF;

            auto wd = m_inotify.add_watch(path, mask);

            auto it = rng::linear_find(m_db.dirs(), path, project<&directory::path>());
            if (it == m_db.dirs().end()) {
                auto it = m_watches.insert({
                    watch_type::mime_dir,
                    wd,
                    string(path),
                    null
                });

                return *it;
            }
            else {
                auto it2 = m_watches.insert({
                    watch_type::mime_dir,
                    wd,
                    string(path),
                    *it
                });

                return *it2;
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    process_event()
    {
        using path::operator/;

        try {
            char buf[1024];

            while (true) {
                auto events = m_inotify.read(buf);
                if (events.empty()) break;

                for (auto const& e: events) {
                    auto it = m_watches.find(e.wd);
                    if (it == m_watches.end() && !bits::any_of(e.mask, IN_IGNORED)) {
                        log::dbg() << [&](auto& s) {
                            s << json::object {
                                { "message", "receive inotify event for unknown wd" },
                                { "event", e },
                            };
                        };
                        continue;
                    }

                    try {
                        if (e.mask & IN_IGNORED) {
                            m_watches.erase(it);
                        }
                        else if (it->type == watch_type::data_dir) {
                            handle_data_dir_event(it, e);
                        }
                        else { // it->type == watch_type::mime_dir
                            handle_mime_dir_event(it, e);
                        }
                    }
                    catch (...) {
                        log::dbg() << [&](auto& s) {
                            s << json::object {
                                { "message", "an exception is ignored" },
                                { "error", std::current_exception() },
                            };
                        };
                    }
                }
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    handle_data_dir_event(auto it, ::inotify_event const& e)
    {
        using path::operator/;

        try {
            auto& w = *it;
            if (bits::any_of(e.mask, IN_CREATE | IN_MOVED_TO)) {
                if (lx::name(e) != "mime" || bits::none_of(e.mask, IN_ISDIR)) return;

                auto p = w.path / "mime";

                auto it = rng::linear_find(m_db.dirs(), p, project<&directory::path>());
                if (it != m_db.dirs().end()) {
                    log::dbg() << [&](auto& s) {
                        s << json::object {
                            { "message", "receive IN_CREATE for a directory that is already registered" },
                            { "event", e },
                            { "path", it->path() }
                        };
                    };
                }
                else {
                    auto& w2 = add_mime_dir_watch(p);
                    w2.dir = m_db.append_directory(p);
                    log::info() << [&](auto& s) {
                        s << json::object {
                            { "message", "mime directory is created" },
                            { "path", p }
                        };
                    };
                }
            }
            else if (bits::any_of(e.mask, IN_DELETE | IN_MOVED_FROM)) {
                if (lx::name(e) == "mime") {
                    auto p = w.path / "mime";
                    m_db.remove_directory(p);
                    log::info() << [&](auto& s) {
                        s << json::object {
                            { "message", "mime directory is removed" },
                            { "path", p }
                        };
                    };
                }
            }
            else if (bits::any_of(e.mask, IN_DELETE_SELF | IN_MOVE_SELF)) {
                m_watches.erase(it);
                log::info() << [&](auto& s) {
                    s << json::object {
                        { "message", "data directory is removed" },
                        { "path", w.path }
                    };
                };
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    handle_mime_dir_event(auto it, ::inotify_event const& e)
    {
        using path::operator/;

        try {
            if (bits::any_of(e.mask, IN_CLOSE_WRITE | IN_MOVED_TO)) {
                if (lx::name(e) != "mime.cache") return;

                if (it->dir) {
                    (*it->dir).cache().reload();
                    log::info() << [&](auto& s) {
                        s << json::object {
                            { "message", "reloading mime.cache" },
                            { "path", it->path }
                        };
                    };
                }
                else {
                    it->dir = m_db.append_directory(it->path);
                    log::info() << [&](auto& s) {
                        s << json::object {
                            { "message", "loading mime.cache" },
                            { "path", it->path }
                        };
                    };
                }
            }
            else if (bits::any_of(e.mask, IN_DELETE | IN_MOVED_FROM)) {
                if (lx::name(e) != "mime.cache") return;

                log::info() << [&](auto& s) {
                    s << json::object {
                        { "message", "mime.cache is removed" },
                        { "path", it->path / "mime.cache" }
                    };
                };

                if (it->dir) {
                    m_db.remove_directory(*it->dir);
                    it->dir = null;
                }
            }
            else if (bits::any_of(e.mask, IN_DELETE_SELF | IN_MOVE_SELF)) {
                log::info() << [&](auto& s) {
                    s << json::object {
                        { "message", "mime directory is removed" },
                        { "path", it->path }
                    };
                };
                m_watches.erase(it);
            }
        }
        catch (...) {
            rethrow_error();
        }
    }
};

database_updater::
database_updater(database& db)
    : m_p { *db.m_p }
{}

database_updater::~database_updater() noexcept = default;

lx::fd_ref database_updater::
event_fd() const noexcept
{
    return m_p->m_inotify.fd();
}

void database_updater::
process_event()
{
    m_p->process_event();
}

} // namespace stream9::xdg::mime
