#ifndef STREAM9_XDG_MIME_DIRECTORY_TCC
#define STREAM9_XDG_MIME_DIRECTORY_TCC

#include "directory.hpp"

#include <stream9/xdg/mime/environment.hpp>
#include <stream9/xdg/mime/namespace.hpp>

namespace stream9::xdg::mime {

inline cstring_view directory::
path() const noexcept
{
    return m_path;
}

inline class cache const& directory::
cache() const noexcept
{
    return *m_cache;
}

inline class cache& directory::
cache() noexcept
{
    return *m_cache;
}

inline std::strong_ordering directory::
operator<=>(directory const& o) const noexcept
{
    return m_path <=> o.m_path;
}

inline bool directory::
operator==(directory const& other) const noexcept
{
    return std::is_eq(*this <=> other);
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_DIRECTORY_TCC
