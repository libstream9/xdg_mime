#include "icons.hpp"

#include <algorithm>

namespace stream9::xdg::mime {

opt<icon> icons::
search(string_view mime_type) const noexcept
{
    using std::ranges::lower_bound;

    opt<icon> rv;

    auto it = lower_bound(*this, mime_type, {},
        [](auto&& icon) { return icon.mime_type(); } );
    if (!(it == end()) && !(mime_type < (*it).mime_type())) {
        rv = *it;
    }

    return rv;
}

} // namespace stream9::xdg::mime
