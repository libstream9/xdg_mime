#ifndef STREAM9_XDG_MIME_GLOB_RECORD_HPP
#define STREAM9_XDG_MIME_GLOB_RECORD_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "glob_flag.hpp"
#include "glob_weight.hpp"

#include <stream9/cstring_view.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

struct glob_record {
    string pattern;
    cstring_view mime_type;
    glob_flags flags;
    glob_weight weight;
};

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_GLOB_RECORD_HPP
