#ifndef STREAM9_XDG_MIME_MAGIC_TCC
#define STREAM9_XDG_MIME_MAGIC_TCC

#include "magic.hpp"

#include "endian.hpp"

namespace stream9::xdg::mime {

struct magic_list {
    card32_t n_matches;
    card32_t max_extent;
    card32_t first_match_offset;
};

struct match_entry {
    card32_t priority;
    card32_t mime_type_offset;
    card32_t n_matchlets;
    card32_t first_matchlet_offset;
};

struct matchlet_entry {
    card32_t range_start;
    card32_t range_length;
    card32_t word_size;
    card32_t value_length;
    card32_t value_offset;
    card32_t mask_offset;
    card32_t n_children;
    card32_t first_child_offset;
};

/*
 * magics
 */
inline magics::
magics(class cache const& c, char const* base, std::uint32_t offset) noexcept
    : m_cache { &c }
    , m_base { base }
    , m_record { reinterpret_cast<magic_list const*>(m_base + offset) }
{}

inline class cache const& magics::
cache() const noexcept
{
    return *m_cache;
}

inline magics::const_iterator magics::
begin() const noexcept
{
    auto* entries = reinterpret_cast<match_entry const*>(
                                       m_base + m_record->first_match_offset);
    return { m_base, &entries[0] };
}

inline magics::const_iterator magics::
end() const noexcept
{
    auto* entries = reinterpret_cast<match_entry const*>(
                                       m_base + m_record->first_match_offset);
    return { m_base, &entries[size()] };
}

inline std::uint32_t magics::
size() const noexcept
{
    return m_record->n_matches;
}

inline std::uint32_t magics::
max_extent() const noexcept
{
    return m_record->max_extent;
}

/*
 * magic
 */
inline magic::
magic(char const* base, match_entry const* entry) noexcept
    : m_base { base }
    , m_record { entry }
{}

inline magic_priority magic::
priority() const noexcept
{
    return m_record->priority.value();
}

inline str::cstring_view magic::
mime_type() const noexcept
{
    return m_base + m_record->mime_type_offset;
}

inline magic::const_iterator magic::
begin() const noexcept
{
    auto* entries = reinterpret_cast<matchlet_entry const*>(
                                    m_base + m_record->first_matchlet_offset);
    return { m_base, &entries[0] };
}

inline magic::const_iterator magic::
end() const noexcept
{
    auto* entries = reinterpret_cast<matchlet_entry const*>(
                                    m_base + m_record->first_matchlet_offset);
    return { m_base, &entries[size()] };
}

inline std::uint32_t magic::
size() const noexcept
{
    return m_record->n_matchlets;
}

/*
 * match
 */
inline match::
match(char const* base, matchlet_entry const* entry) noexcept
    : m_base { base }
    , m_record { entry }
{}

inline std::uint32_t match::
range_start() const noexcept
{
    return m_record->range_start;
}

inline std::uint32_t match::
range_length() const noexcept
{
    return m_record->range_length;
}

inline std::uint32_t match::
word_size() const noexcept
{
    return m_record->word_size;
}

inline match::value_t match::
value() const noexcept
{
    auto* data = m_base + m_record->value_offset;

    return { data, m_record->value_length.value() };
}

inline opt<match::value_t> match::
mask() const noexcept
{
    auto offset = m_record->mask_offset;
    if (offset == 0) {
        return null;
    }
    else {
        auto* data = m_base + offset;

        return value_t { data, m_record->value_length.value() };
    }
}

inline match::const_iterator match::
begin() const noexcept
{
    auto* entries = reinterpret_cast<matchlet_entry const*>(
                                       m_base + m_record->first_child_offset);

    return { m_base, &entries[0] };
}

inline match::const_iterator match::
end() const noexcept
{
    auto* entries = reinterpret_cast<matchlet_entry const*>(
                                       m_base + m_record->first_child_offset);

    return { m_base, &entries[size()] };
}

inline std::uint32_t match::
size() const noexcept
{
    return m_record->n_children;
}

/*
 * magic_iterator
 */
inline magic_iterator::
magic_iterator(char const* base, match_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline magic magic_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void magic_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void magic_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void magic_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline magic_iterator::difference_type magic_iterator::
distance_to(magic_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool magic_iterator::
equal(magic_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering magic_iterator::
compare(magic_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

/*
 * match_iterator
 */
inline match_iterator::
match_iterator(char const* base, matchlet_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline match match_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void match_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void match_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void match_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline match_iterator::difference_type match_iterator::
distance_to(match_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool match_iterator::
equal(match_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering match_iterator::
compare(match_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_MAGIC_TCC
