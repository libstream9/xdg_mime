#ifndef STREAM9_XDG_MIME_TREEMAGIC_HPP
#define STREAM9_XDG_MIME_TREEMAGIC_HPP

#include "treemagic_priority.hpp"

#include <stream9/xdg/mime/error.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include <cstdint>

#include <stream9/array.hpp>
#include <stream9/bit_flags.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/optional.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime {

class database;

class treematch
{
public:
    enum class type {
        any,
        file,
        directory,
        link,
    };

    enum class option : std::uint16_t {
        none = 0,
        executable = 1,
        match_case = 2,
        non_empty = 4,
    };

public:
    treematch(string_view path,
              enum type,
              bit_flags<enum option>,
              opt<string_view> mime_type);

    treematch(string_view path,
              enum type,
              bit_flags<enum option>,
              opt<string_view> mime_type,
              array<treematch>);

    // query
    cstring_view path() const noexcept;
    enum type type() const noexcept;
    bool match_case() const noexcept;
    bool executable() const noexcept;
    bool non_empty() const noexcept;
    cstring_view mime_type() const noexcept;

    array<treematch> const& children() const noexcept;

    bool execute(database const&, cstring_ptr const& root_path) const;

private:
    string m_path;
    enum type m_type;
    bit_flags<enum option> m_options;
    string m_mime_type;
    array<treematch> m_children;
};

class treemagic : public stream9::ranges::range_facade<treemagic>
{
public:
    treemagic(string_view mime_type, int priority, array<treematch>);

    // accessor
    auto begin() const noexcept { return m_matches.begin(); }
    auto end() const noexcept { return m_matches.end(); }

    // query
    string const& mime_type() const noexcept;
    treemagic_priority priority() const noexcept;

    bool match(database const&, cstring_ptr const& root_path) const;

private:
    string m_mime_type;
    treemagic_priority m_priority;
    array<treematch> m_matches;
};

class treemagics : public stream9::ranges::range_facade<treemagics>
{
public:
    treemagics(cstring_ptr const& dir_path);

    // accessor
    auto begin() const noexcept { return m_magics.begin(); }
    auto end() const noexcept { return m_magics.end(); }

    // query
    void search(array<treemagic const*>& result,
                database const&,
                cstring_ptr const& root_path) const;

private:
    array<treemagic> m_magics;
};

} // namespace stream9::xdg::mime

#include "treemagic_match.hpp"
#include "treemagic.tcc"

#endif // STREAM9_XDG_MIME_TREEMAGIC_HPP
