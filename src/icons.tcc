#ifndef STREAM9_XDG_MIME_ICONS_TCC
#define STREAM9_XDG_MIME_ICONS_TCC

#include "icons.hpp"

namespace stream9::xdg::mime {

struct icon_list_entry {
    card32_t mime_type_offset;
    card32_t icon_name_offset;
};

struct icon_list {
    card32_t n_icons;
    icon_list_entry entries[1];
};

/*
 * icon
 */
inline icon::
icon(char const* base, icon_list_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline str::cstring_view icon::
mime_type() const noexcept
{
    return m_base + m_entry->mime_type_offset;
}

inline str::cstring_view icon::
name() const noexcept
{
    return m_base + m_entry->icon_name_offset;
}

template<std::size_t I>
    requires (I < 2)
decltype(auto) icon::
get() const noexcept
{
    if constexpr (I == 0) {
        return mime_type();
    }
    else {
        return name();
    }
}

/*
 * icons
 */
inline icons::
icons(char const* base, card32_t offset) noexcept
    : m_base { base }
    , m_record { reinterpret_cast<icon_list const*>(m_base + offset) }
{}

inline icons::const_iterator icons::
begin() const noexcept
{
    return { m_base, &m_record->entries[0] };
}

inline icons::const_iterator icons::
end() const noexcept
{
    return { m_base, &m_record->entries[size()] };
}

inline std::uint32_t icons::
size() const noexcept
{
    return m_record->n_icons;
}

/*
 * icon_iterator
 */
inline icon_iterator::
icon_iterator(char const* base, icon_list_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline icon icon_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void icon_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void icon_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void icon_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline icon_iterator::difference_type icon_iterator::
distance_to(icon_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool icon_iterator::
equal(icon_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering icon_iterator::
compare(icon_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_ICONS_TCC
