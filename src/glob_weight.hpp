#ifndef STREAM9_XDG_MIME_GLOB_WEIGHT_HPP
#define STREAM9_XDG_MIME_GLOB_WEIGHT_HPP

#include <cstdint>

#include <stream9/safe_integer.hpp>

namespace stream9::xdg::mime {

using glob_weight = safe_integer<std::int32_t, 0, 100>;

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_GLOB_WEIGHT_HPP
