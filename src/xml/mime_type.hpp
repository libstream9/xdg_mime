#ifndef STREAM9_XDG_TEST_MIME_XML_MIME_TYPE_HPP
#define STREAM9_XDG_TEST_MIME_XML_MIME_TYPE_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include <cstdint>

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/node.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime::xml {

class mime_type
{
public:
    struct text;
    struct glob;
    struct magic;
    struct match;
    struct treemagic;
    struct treematch;
    struct root_xml;

public:
    mime_type(string_view name, string_view comment);
    mime_type(cstring_ptr const& xml_path);
    ~mime_type() noexcept;

    // query
    cstring_view name() const;

    cstring_view comment(string_view language) const;
    array<struct text> comments() const;

    array<cstring_view> aliases() const;
    array<cstring_view> base_classes() const;

    cstring_view icon() const;
    cstring_view generic_icon() const;

    cstring_view acronym(string_view language) const;
    array<struct text> acronyms() const;

    cstring_view expanded_acronym(string_view language) const;
    array<struct text> expanded_acronyms() const;

    array<struct glob> globs() const;
    array<struct magic> magics() const;
    array<struct treemagic> treemagics() const;
    array<struct root_xml> root_xmls() const;

    bool glob_deleteall() const;
    bool magic_deleteall() const;

    string to_xml() const;

    // modifier
    void set_comment(string_view text, string_view language);
    void append_alias(string_view);
    void append_base_class(string_view);
    void set_icon(string_view name);
    void set_generic_icon(string_view name);
    void set_acronym(string_view text, string_view language);
    void set_expanded_acronym(string_view text, string_view language);

    void append_glob(string_view pattern, std::uint32_t weight = 50);
    void append_magic(magic const&);
    void append_treemagic(treemagic const&);
    void append_root_xml(string_view namespace_uri, string local_name);

    void set_glob_deleteall(bool = true);
    void set_magic_deleteall(bool = true);

    void merge(mime_type const&);

private:
    friend class mime_info;
    class impl;
    node<impl> m_p;
};

struct mime_type::text
{
    cstring_view text;
    cstring_view lang;
};

struct mime_type::glob
{
    cstring_view pattern;
    std::uint32_t weight;

    auto operator<=>(glob const&) const = default;
};

struct mime_type::match
{
    cstring_view type;
    cstring_view offset;
    cstring_view value;
    cstring_view mask;
    array<match> children;
};

struct mime_type::magic
{
    std::uint32_t priority;
    array<match> matches;
};

struct mime_type::treemagic
{
    std::uint32_t priority;
    array<treematch> matches;
};

struct mime_type::treematch
{
    cstring_view path;
    cstring_view type;
    cstring_view match_case;
    cstring_view executable;
    cstring_view non_empty;
    cstring_view mime_type;
    array<treematch> children;
};

struct mime_type::root_xml
{
    cstring_view namespace_uri;
    cstring_view local_name;
};

} // namespace stream9::xdg::mime::xml

#endif // STREAM9_XDG_TEST_MIME_XML_MIME_TYPE_HPP
