#ifndef STREAM9_XDG_MIME_SUBCLASSES_HPP
#define STREAM9_XDG_MIME_SUBCLASSES_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "endian.hpp"

#include <compare>
#include <cstdint>
#include <iterator>
#include <ranges>

#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime {

class subclass_iterator;
class parents;
class parent_iterator;

struct parent_list;
struct parent_list_entry;

/**
 * Represents subclass entry
 *
 * @model std::copy_constructible
 * @model std::move_constructible
 * @model std::destructible
 * @model std::assignable_from<T const&>
 * @model std::assignable_from<T&&>
 */
class subclass
{
public:
    subclass(char const* base, parent_list_entry const*) noexcept;

    cstring_view mime_type() const noexcept;
    class parents parents() const noexcept;

    template<std::size_t>
    decltype(auto) get() const noexcept;

private:
    char const* m_base; // non-null
    parent_list_entry const* m_entry; // non-null
};

/**
 * Represents cached data from file <MIME>/subclasses
 *
 * Data is organized as sorted range of subclass, elements are sorted
 * lexicographically by subclass.mime_type().
 *
 * @model std::copy_constructible
 * @model std::move_constructible
 * @model std::destructible
 * @model std::assignable_from<T const&>
 * @model std::assignable_from<T&&>
 * @model std::ranges::random_access_range
 * @model std::ranges::sized_range
 * @model std::ranges::borrowed_range
 * @model std::ranges::common_range
 */
class subclasses : public stream9::ranges::range_facade<subclasses>
{
public:
    using const_iterator = subclass_iterator;

public:
    subclasses(char const* base, card32_t offset) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    parents search(string_view mime_type) const noexcept;

private:
    friend class subclass_iterator;

    char const* m_base; // non-null
    parent_list const* m_list; // non-null
};

/**
 * @model std::random_access_iterator
 */
class subclass_iterator : public iter::iterator_facade<subclass_iterator,
                                            std::random_access_iterator_tag,
                                            subclass,
                                            subclass>
{
public:
    subclass_iterator() = default;
    subclass_iterator(char const* base, parent_list_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    subclass dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(subclass_iterator const&) const noexcept;

    bool equal(subclass_iterator const&) const noexcept;

    std::strong_ordering compare(subclass_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    parent_list_entry const* m_entry = nullptr;
};

/**
 * Represents cached data from file <MIME>/subclasses
 *
 * Data is organized as range of mime_types.
 *
 * @model std::copy_constructible
 * @model std::move_constructible
 * @model std::destructible
 * @model std::assignable_from<T const&>
 * @model std::assignable_from<T&&>
 * @model std::ranges::random_access_range
 * @model std::ranges::sized_range
 * @model std::ranges::borrowed_range
 * @model std::ranges::common_range
 */
class parents : public stream9::ranges::range_facade<parents>
{
public:
    using const_iterator = parent_iterator;

public:
    parents() = default;

    parents(char const* base, char const* record) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

private:
    friend class parent_iterator;

    struct record {
        card32_t n_parents;
        card32_t mime_type_offset[1];
    };

    char const* m_base = nullptr;
    record const* m_record = nullptr;
};

/**
 * @model std::random_access_iterator
 */
class parent_iterator : public iter::iterator_facade<parent_iterator,
                                                     std::random_access_iterator_tag,
                                                     char const*,
                                                     char const* >
{
public:
    parent_iterator() = default;
    parent_iterator(char const* base, card32_t const*) noexcept;

private:
    friend class iter::iterator_core_access;

    char const* dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(parent_iterator const&) const noexcept;

    bool equal(parent_iterator const&) const noexcept;

    std::strong_ordering compare(parent_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    card32_t const* m_entry = nullptr;
};

} // namespace stream9::xdg::mime

#include "subclasses.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<stream9::xdg::mime::subclasses> = true;

template<>
inline constexpr bool enable_borrowed_range<stream9::xdg::mime::parents> = true;

} // namespace std::ranges

namespace std {

template<>
struct tuple_size<stream9::xdg::mime::subclass>
    : integral_constant<size_t, 2> {};

template<>
struct tuple_element<0, stream9::xdg::mime::subclass>
{
    using type = char const*;
};

template<>
struct tuple_element<1, stream9::xdg::mime::subclass>
{
    using type = stream9::xdg::mime::parents;
};

} // namespace std

#endif // STREAM9_XDG_MIME_SUBCLASSES_HPP
