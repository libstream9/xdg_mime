#include "treemagic.hpp"

#include <stream9/xdg/mime/database.hpp>
#include <stream9/xdg/mime/environment.hpp>

#include "treemagic_match.hpp"

#include <algorithm>
#include <cctype>
#include <tuple>
#include <utility>

#include <stream9/bit_flags.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/push_back.hpp>

namespace stream9::xdg::mime {

    namespace parser {

        struct error {
            string msg;
            char const* it;
        };

        static int
        priority(auto& it, auto const& end)
        {
            if (it == end) {
                throw error("priority is expected", it);
            }
            if (!std::isdigit(*it)) {
                throw error("digit is expected", it);
            }

            int result = 0;

            for (; it != end; ++it) {
                if (!std::isdigit(*it)) break;

                result = (result * 10) + (*it - '0');
            }

            return result;
        }

        static int
        indent(auto& it, auto const& end)
        {
            int result = 0;

            for (; it != end; ++it) {
                auto c = *it;

                if (!std::isdigit(c)) break;

                result = (result * 10) + (c - '0');
            }

            return result;
        }

        static string_view
        path(auto& it, auto const& end)
        {
            if (it == end) {
                throw error("path is expected", it);
            }

            if (*it != '"') {
                throw error("\" is expected", it);
            }
            ++it;

            auto from = it;
            for (; it != end; ++it) {
                auto c = *it;

                if (c == '\"') break;
            }

            if (from == it) {
                throw error("path can't be empty", it);
            }
            if (it == end) {
                throw error("\" is expected", it);
            }
            auto to = it;
            ++it;

            return { from, to };
        }

        static enum treematch::type
        type(auto& it, auto const& end)
        {
            auto from = it;
            for (; it != end; ++it) {
                if (*it == '\n' || *it == ',') break;
            }

            string_view t { from, it };
            if (t == "file") {
                return treematch::type::file;
            }
            else if (t == "directory") {
                return treematch::type::directory;
            }
            else if (t == "any") {
                return treematch::type::any;
            }
            else {
                throw error("unknown type", it);
            }
        }

        static opt<string_view>
        option(auto& it, auto const& end)
        {
            if (it == end || *it == '\n') return null;

            if (*it != ',') {
                throw error(", is expected", it);
            }
            ++it;

            auto from = it;
            for (; it != end; ++it) {
                if (*it == ',' || *it == '\n') break;
            }

            return string_view { from, it };
        }

        // "executable" | "match-case" | "non-empty" | mime-type
        static auto
        options(auto& it, auto const& end)
        {
            using O = treematch::option;

            bit_flags<O> opts = O::none;
            opt<string_view> o_mime_type;

            while (true) {
                auto o_o = option(it, end);
                if (!o_o) break;

                if (*o_o == "executable") {
                    opts |= O::executable;
                }
                else if (*o_o == "match-case") {
                    opts |= O::match_case;
                }
                else if (*o_o == "non-empty") {
                    opts |= O::non_empty;
                }
                else {
                    if (o_mime_type) {
                        throw error("multiple mime-type in option", it);
                    }
                    o_mime_type = *o_o;
                }
            }

            return std::make_tuple(opts, o_mime_type);
        }

        // ?indent '>' path '=' type *(',' option) '\n'
        static auto
        magic_match(auto& it, auto const& end)
        {
            if (it == end) {
                throw error("unexpected EOF", it);
            }

            int idnt = parser::indent(it, end);

            if (*it != '>') {
                throw error("> is expected", it);
            }
            ++it;

            auto p = parser::path(it, end);

            if (it == end || *it != '=') {
                throw error("syntax error", it);
            }
            ++it;

            auto t = parser::type(it, end);

            auto [opts, o_mime_type] = parser::options(it, end);

            if (*it != '\n') {
                throw error("\\n is expected", it);
            }
            ++it;

            return std::make_tuple(idnt, p, t, opts, o_mime_type);
        }

        static array<treematch>
        magic_matches(auto& it, auto const& end, int indent)
        {
            array<treematch> result;

            while (it != end && *it != '[') {
                auto save = it;
                auto [i, path, type, opts, o_mime_type] = magic_match(it, end);

                if (i == indent) {
                    emplace_back(result, path, type, opts, o_mime_type);
                }
                else if (i > indent) {
                    auto children = magic_matches(it, end, i);
                    emplace_back(result, path, type, opts, o_mime_type, std::move(children));
                }
                else { // i < index
                    it = save;
                    break;
                }
            }

            return result;
        }

        // '[' priority ':' mime-type ']' '\n'
        static auto
        magic_header(auto& it, auto const& end)
        {
            if (it == end || *it != '[') {
                throw error("magic header is expected", it);
            }
            ++it;

            auto pri = parser::priority(it, end);
            if (it == end || *it != ':') {
                throw error(": is expected", it);
            }
            ++it;

            auto from = it;
            for (; it != end; ++it) {
                if (*it == ']') break;
            }
            if (*it != ']') {
                throw error("] is expected", it);
            }
            if (from == it) {
                throw error("empty mime_type", it);
            }
            string_view mime_type { from, it };
            ++it;

            if (*it != '\n') {
                throw error("\\n is expected", it);
            }
            ++it;

            return std::make_tuple(pri, mime_type);
        }

        static treemagic
        magic(auto& it, auto const& end)
        {
            auto [priority, mime_type] = magic_header(it, end);

            auto matches = magic_matches(it, end, 0);
            if (matches.empty()) {
                throw error("treemagic with no treematch", it);
            }

            return { mime_type, priority, std::move(matches) };
        }

        static void
        file_header(auto& it, auto const& end)
        {
            using namespace std::literals;
            auto magic = "MIME-TreeMagic\0\n"sv;

            auto it2 = magic.begin();
            auto end2 = magic.end();

            for (; it != end && it2 != end2; ++it, ++it2) {
                if (*it != *it2) break;
            }
            if (it2 != end2) {
                throw error("invalid file header", it);
            }
        }

        static array<treemagic>
        treemagics(auto& it, auto const& end)
        {
            array<treemagic> result;

            file_header(it, end);

            while (it != end) {
                push_back(result, magic(it, end));
            }

            return result;
        }

    } // namespace parser

static opt<string>
imatch_path(cstring_ptr const& path)
{
    try {
        return env().real_path_icase(path);
    }
    catch (...) {
        rethrow_error();
    }
}

static opt<string>
match_path(cstring_ptr const& path)
{
    try {
        if (env().is_file_exists(path)) {
            return path;
        }
        else {
            return null;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static bool
match_type(cstring_ptr const& path, enum treematch::type type)
{
    try {
        using t = decltype(type);

        switch (type) {
            case t::file:
                return env().is_regular_file(path);
            case t::directory:
                return env().is_directory(path);
            case t::link:
                return env().is_symbolic_link(path);
            case t::any:
            default:
                return true;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static bool
match_executable(cstring_ptr const& path, bool executable) noexcept
{
    if (!executable) return true;

    try {
        return env().is_executable(path);
    }
    catch (...) {
        return false;
    }
}

static bool
match_non_empty(cstring_ptr const& path, bool non_empty) noexcept
{
    if (!non_empty) return true;

    try {
        return env().is_non_empty_directory(path);
    }
    catch (...) {
        return false;
    }
}

static bool
match_mime_type(database const& db,
                cstring_ptr const& path,
                string_view mime_type) noexcept
{
    try {
        if (mime_type.empty()) return true;

        auto m = db.find_mime_type_for_file(path);
        return m.name() == mime_type;
    }
    catch (...) {
        return false;
    }
}

/*
 * treematch
 */
bool treematch::
execute(database const& db, cstring_ptr const& root_path) const
{
    using path::operator/;

    try {
        assert(path::is_absolute(root_path));
        auto const& p = root_path / string_view(path());

        auto o_path = match_case() ? match_path(p) : imatch_path(p);
        if (!o_path) return false;

        if (!match_type(*o_path, m_type)) return false;

        if (!match_executable(*o_path, executable())) return false;

        if (!match_non_empty(*o_path, non_empty())) return false;

        if (!match_mime_type(db, *o_path, m_mime_type)) return false;

        if (m_children.empty()) {
            return true;
        }
        else {
            return std::ranges::any_of(m_children,
                [&](auto&& match) { return match.execute(db, root_path); } );
        }
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * treemagic
 */
bool treemagic::
match(database const& db, cstring_ptr const& root_path) const
{
    try {
        return std::ranges::any_of(
            *this, [&](auto&& match) { return match.execute(db, root_path); } );
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * treemagics
 */
treemagics::
treemagics(cstring_ptr const& dir_path)
{
    using path::operator/;

    try {
        auto const& path = dir_path / "treemagic";

        auto content = env().load_file(path);
        if (content.empty()) {
            throw_error(errc::parse_error, {
                { "path", path },
                { "description", "file is empty" },
            });
        }

        auto it = &content[0];
        auto end = it + content.size();
        try {
            m_magics = parser::treemagics(it, end);
        }
        catch (parser::error const& e) {
            throw_error(
                errc::parse_error, {
                    { "path", path },
                    { "pos", e.it - &content[0] },
                    { "description", e.msg },
                }
            );
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void treemagics::
search(array<treemagic const*>& result,
       database const& db,
       cstring_ptr const& root_path) const
{
    try {
        for (auto const& magic: *this) {
            if (magic.match(db, root_path)) {
                push_back(result, &magic);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
