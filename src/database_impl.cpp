#include "database_impl.hpp"

#include <stream9/xdg/mime/database.hpp>
#include <stream9/xdg/mime/error.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include "cache.hpp"
#include "glob_match.hpp"
#include "magic_match.hpp"
#include "subclasses.hpp"
#include "treemagic_match.hpp"
#include "rapidxml_util.hpp"

#include <cctype>
#include <ranges>

#include "rapidxml/rapidxml.hpp"

#include <stream9/emplace_back.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/log.hpp>
#include <stream9/ranges/linear_find.hpp>
#include <stream9/ranges/contains.hpp>
#include <stream9/string.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace stream9::xdg::mime {

static bool
is_text(file_data const& data) noexcept
{
    using std::ranges::views::take;

    if (data.empty()) return false;

    for (auto const c: take(data, 128)) {
        if (std::iscntrl(c)) {
            return false;
        }
    }

    return true;
}

static bool
contains_directory(array<directory> const& dirs, string_view path)
{
    return rng::contains(dirs, path, project<&directory::path>());
}

/*
 * database_impl
 */
mime_type database_impl::
default_mime_type() const
{
    try {
        return default_mime_type("application/octet-stream");
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type database_impl::
default_mime_type(string_view name) const
{
    try {
        auto o_mime = m_db.find_mime_type_for_name(name);
        if (!o_mime) {
            return { m_db, name };
        }

        return std::move(*o_mime);
    }
    catch (...) {
        rethrow_error({
            { "name", name }
        });
    }
}

mime_type database_impl::
default_mime_type_for_data(file_data const& data) const
{
    try {
        auto name =
            is_text(data) ? "text/plain" : "application/octet-stream";

        return default_mime_type(name);
    }
    catch (...) {
        rethrow_error();
    }
}

std::uint32_t database_impl::
get_max_extent() const noexcept
{
    std::uint32_t max = 128;

    for (auto const& dir: m_dirs) {
        auto e = dir.max_extent();
        if (e > max) {
            max = e;
        }
    }

    return max;
}

array<glob_match> database_impl::
glob_search(string_view filename) const
{
    try {
        array<glob_match> result;

        for (auto const& dir: m_dirs) {
            dir.glob_search(result, filename);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<magic_match> database_impl::
magic_search(file_data const& data) const
{
    try {
        array<magic_match> result;
        if (data.empty()) return result;

        for (auto const& dir: m_dirs) {
            dir.magic_search(result, data);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database_impl::
xml_search(string_view text) const
{
    try {
        string copy { text };
        rapidxml::xml_document<> doc;
        doc.parse<0>(copy.data());

        auto& root = root_element(doc);
        auto namespace_uri = get_attribute(root, "xmlns");
        auto local_name = name(root);

        return xml_search(namespace_uri, local_name);
    }
    catch (rapidxml::parse_error const&) {
        throw_error(errc::parse_error, {
            { "text", text }
        });
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database_impl::
xml_search(string_view namespace_uri,
           string_view local_name) const
{
    try {
        database::mime_type_set result;

        for (auto const& dir: m_dirs) {
            auto* name =
                dir.xml_namespaces_search(namespace_uri, local_name);
            if (name) {
                result.insert({ m_db, name });
            }
        }

        if (result.empty()) {
            result.insert(default_mime_type("application/xml"));
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type database_impl::
select_match(array<glob_match>& glob_matches,
             array<magic_match>& magic_matches,
             file_data const& data)
{
    using std::ranges::sort;

    try {
        for (auto const& gm: glob_matches) {
            mime_type gm_type { m_db, gm.mime_type() };

            for (auto const& mm: magic_matches) {
                if (gm_type.name() == mm.mime_type()) {
                    return gm_type;
                }
                if (gm_type.is_subclass_of(mm.mime_type())) {
                    return gm_type;
                }
            }
        }

        if (glob_matches.empty()) {
            if (magic_matches.empty()) {
                return default_mime_type_for_data(data);
            }
            else {
                sort(magic_matches, std::greater());
                auto const& match = magic_matches.front();

                return mime_type { m_db, match.mime_type() };
            }
        }
        else {
            sort(glob_matches, std::greater());
            auto const& match = glob_matches.front();

            return mime_type { m_db, match.mime_type() };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

array<treemagic_match> database_impl::
filesystem_search(cstring_ptr const& root) const
{
    using std::ranges::sort;

    try {
        array<treemagic_match> matches;

        for (auto const& dir: m_dirs) {
            dir.treemagic_search(matches, m_db, root);
        }

        sort(matches, std::greater());

        return matches;
    }
    catch (...) {
        rethrow_error();
    }
}

opt<directory&>  database_impl::
append_directory(cstring_ptr const& dir_path)
{
    using std::ranges::none_of;
    using path::operator/;

    opt<directory&> rv;

    try {
        if (!contains_directory(m_dirs, dir_path)) {
            if (env().is_regular_file(dir_path / "mime.cache")) {
                auto& d = stream9::emplace_back(m_dirs, dir_path);

                rv = d;
            }
        }
    }
    catch (...) {
        rethrow_error();
    }

    return rv;
}

void database_impl::
remove_directory(cstring_ptr const& dir_path)
{
    try {
        auto it = rng::linear_find(m_dirs, dir_path, project<&directory::path>());
        if (it != m_dirs.end()) {
            m_dirs.erase(it);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void database_impl::
remove_directory(directory const& dir)
{
    try {
        stream9::erase_if(m_dirs, [&](auto&& d) { return &d == &dir; });
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
