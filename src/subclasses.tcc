#ifndef STREAM9_XDG_MIME_SUBCLASSES_TCC
#define STREAM9_XDG_MIME_SUBCLASSES_TCC

#include "subclasses.hpp"

namespace stream9::xdg::mime {

struct parent_list_entry {
    card32_t mime_type_offset;
    card32_t parents_offset;
};

struct parent_list {
    card32_t n_entries;
    parent_list_entry entries[1];
};

/*
 * subclass
 */
inline subclass::
subclass(char const* base, parent_list_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline cstring_view subclass::
mime_type() const noexcept
{
    return m_base + m_entry->mime_type_offset;
}

inline class parents subclass::
parents() const noexcept
{
    return { m_base, m_base + m_entry->parents_offset };
}

template<std::size_t I>
decltype(auto) subclass::
get() const noexcept
{
    if constexpr (I == 0)
        return mime_type();
    else
        return parents();
}

/*
 * subclasses
 */
inline subclasses::
subclasses(char const* base, card32_t offset) noexcept
    : m_base { base }
    , m_list { reinterpret_cast<parent_list const*>(base + offset) }
{}

inline subclasses::const_iterator subclasses::
begin() const noexcept
{
    return { m_base, &m_list->entries[0] };
}

inline subclasses::const_iterator subclasses::
end() const noexcept
{
    return { m_base, &m_list->entries[size()] };
}

inline std::uint32_t subclasses::
size() const noexcept
{
    return m_list->n_entries;;
}

/*
 * subclass_iterator
 */
inline subclass_iterator::
subclass_iterator(char const* base, parent_list_entry const* e) noexcept
    : m_base { base }
    , m_entry { e }
{}

inline subclass subclass_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void subclass_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void subclass_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void subclass_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline subclass_iterator::difference_type subclass_iterator::
distance_to(subclass_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool subclass_iterator::
equal(subclass_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering subclass_iterator::
compare(subclass_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

/*
 * parents
 */
inline parents::
parents(char const* base, char const* record) noexcept
    : m_base { base }
    , m_record { reinterpret_cast<struct record const*>(record) }
{}

inline parents::const_iterator parents::
begin() const noexcept
{
    if (m_base) {
        return { m_base, &m_record->mime_type_offset[0] };
    }
    else {
        return {};
    }
}

inline parents::const_iterator parents::
end() const noexcept
{
    if (m_base) {
        return { m_base, &m_record->mime_type_offset[size()] };
    }
    else {
        return {};
    }
}

inline std::uint32_t parents::
size() const noexcept
{
    if (m_base) {
        return m_record->n_parents;
    }
    else {
        return 0;
    }
}

/*
 * parent_iterator
 */
inline parent_iterator::
parent_iterator(char const* base, card32_t const* ent) noexcept
    : m_base { base }
    , m_entry { ent }
{}

inline char const* parent_iterator::
dereference() const noexcept
{
    return m_base + *m_entry;
}

inline void parent_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void parent_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void parent_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline parent_iterator::difference_type parent_iterator::
distance_to(parent_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool parent_iterator::
equal(parent_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering parent_iterator::
compare(parent_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_SUBCLASSES_TCC
