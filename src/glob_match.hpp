#ifndef STREAM9_XDG_MIME_GLOB_MATCH_HPP
#define STREAM9_XDG_MIME_GLOB_MATCH_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "glob_flag.hpp"
#include "glob_weight.hpp"

#include <compare>
#include <cstdint>
#include <iosfwd>

#include <stream9/cstring_view.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

class directory;
class mime_type;

class glob_match
{
public:
    glob_match(string pattern,
               cstring_view mime_type,
               glob_flags flags,
               glob_weight weight,
               class directory const&);

    // accessor
    cstring_view mime_type() const noexcept;
    cstring_view pattern() const noexcept;
    glob_flags flags() const noexcept;
    glob_weight weight() const noexcept;

    class directory const& directory() const noexcept;

    // query
    bool has_deleteall() const noexcept;

    // modifier
    void set_directory(class directory const&);

    // compare
    bool operator==(glob_match const&) const = default;
    std::strong_ordering operator<=>(glob_match const&) const noexcept;

private:
    string m_pattern;
    cstring_view m_mime_type; // non-null
    glob_flags m_flags;
    glob_weight m_weight;
    class directory const* m_dir; // non-null
};

std::ostream& operator<<(std::ostream&, glob_match const&);

} // namespace stream9::xdg::mime

#include "glob_match.tcc"

#endif // STREAM9_XDG_MIME_GLOB_MATCH_HPP
