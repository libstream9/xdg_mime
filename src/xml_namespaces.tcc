#ifndef STREAM9_XDG_MIME_XML_NAMESPACES_TCC
#define STREAM9_XDG_MIME_XML_NAMESPACES_TCC

#include "xml_namespaces.hpp"

namespace stream9::xdg::mime {

struct namespace_entry {
    card32_t namespace_uri_offset;
    card32_t local_name_offset;
    card32_t mime_type_offset;
};

struct namespace_list {
    card32_t n_namespaces;
    namespace_entry entries[1];
};

/*
 * xml_namespace
 */
inline xml_namespace::
xml_namespace(char const* base, namespace_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline cstring_view xml_namespace::
namespace_uri() const noexcept
{
    return m_base + m_entry->namespace_uri_offset;
}

inline cstring_view xml_namespace::
local_name() const noexcept
{
    return m_base + m_entry->local_name_offset;
}

inline cstring_view xml_namespace::
mime_type() const noexcept
{
    return m_base + m_entry->mime_type_offset;
}

template<std::size_t I>
    requires (I < 3)
decltype(auto) xml_namespace::
get() const noexcept
{
    if constexpr (I == 0) {
        return namespace_uri();
    }
    else if constexpr (I == 1) {
        return local_name();
    }
    else {
        return mime_type();
    }
}

/*
 * xml_namespaces
 */
inline xml_namespaces::
xml_namespaces(char const* base, card32_t offset) noexcept
    : m_base { base }
    , m_record { reinterpret_cast<namespace_list const*>(m_base + offset) }
{}

inline xml_namespaces::const_iterator xml_namespaces::
begin() const noexcept
{
    return { m_base, &m_record->entries[0] };
}

inline xml_namespaces::const_iterator xml_namespaces::
end() const noexcept
{
    return { m_base, &m_record->entries[size()] };
}

inline std::uint32_t xml_namespaces::
size() const noexcept
{
    return m_record->n_namespaces;
}

/*
 * xml_namespace_iterator
 */
inline xml_namespace_iterator::
xml_namespace_iterator(char const* base, namespace_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline xml_namespace xml_namespace_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void xml_namespace_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void xml_namespace_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void xml_namespace_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline xml_namespace_iterator::difference_type xml_namespace_iterator::
distance_to(xml_namespace_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool xml_namespace_iterator::
equal(xml_namespace_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering xml_namespace_iterator::
compare(xml_namespace_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_XML_NAMESPACES_TCC
