#include "reverse_suffix_tree.hpp"

#include "cache.hpp"
#include "glob_record.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <algorithm>

#include <stream9/emplace_back.hpp>
#include <stream9/optional.hpp>
#include <stream9/strings/back.hpp>
#include <stream9/strings/view/to_lower.hpp>
#include <stream9/strings/view/remove_suffix.hpp>

namespace stream9::xdg::mime {

static char
extract_character(tree_node const& n) noexcept
{
    struct visitor {
        char operator()(tree_branch_node const& n) {
            return n.character();
        }
        char operator()(tree_leaf_node const&) {
            return 0;
        }
    };

    return std::visit(visitor(), n);
}

template<typename It>
opt<tree_branch_node>
find_branch(It begin, It end, char c) noexcept
{
    using std::ranges::lower_bound;

    opt<tree_branch_node> rv;

    auto it = lower_bound(begin, end, c, {}, extract_character);
    if (it != end) {
        auto node = *it;
        auto* const branch = std::get_if<tree_branch_node>(&node);
        if (branch && !(c < branch->character())) {
            rv = *branch;
        }
    }

    return rv;
}

template<typename It>
void
visit_leaves(array<glob_record>& result,
             It& it, It end, glob_flag flag,
             string_view suffix)
{
    try {
        for (; it != end; ++it) {
            auto const& node = *it;
            auto* leaf = std::get_if<tree_leaf_node>(&node);
            if (leaf == nullptr) break;
            if ((leaf->flags() & glob_flag::case_sensitive) ^ flag) continue;

            string pattern = '*';
            pattern.append(suffix);

            emplace_back(result,
                std::move(pattern),
                leaf->mime_type(),
                leaf->flags(),
                leaf->weight()
            );
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Branch, typename S>
void
traverse(array<glob_record>& result,
         Branch const& b, S const& name, glob_flag flag,
         string& suffix)
{
    try {
        if (name.empty()) return;
        if (b.empty()) return;

        auto it = b.begin(), end = b.end();

        visit_leaves(result, it, end, flag, suffix);

        auto c = str::back(name);
        auto o_b = find_branch(it, end, c);
        if (o_b) {
            suffix.prepend(c);
            traverse(result, *o_b, str::views::remove_suffix(name, 1), flag, suffix);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Branch, typename S>
void
traverse(array<glob_record>& result,
         Branch const& b, S const& name, glob_flag flag)
{
    try {
        string suffix;
        traverse(result, b, name, flag, suffix);
    }
    catch (...) {
        rethrow_error();
    }
}

void suffix_tree::
search(array<glob_record>& result,
       string_view filename) const
{
    try {
        // case insensitive search
        traverse(result, *this, str::views::to_lower(filename), glob_flag::none);

        // case sensitive search
        traverse(result, *this, filename, glob_flag::case_sensitive);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
