# stream9::mime

A C++20 implementation of freedesktop.org Shared MIME-Info Database Specification.

## Based Specification

- [Shared MIME-Info Database Specification version 0.21](https://specifications.freedesktop.org/shared-mime-info-spec/shared-mime-info-spec-0.21.html)

