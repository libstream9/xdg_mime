#ifndef STREAM9_XDG_MIME_TYPE_HPP
#define STREAM9_XDG_MIME_TYPE_HPP

#include "mime/mime_type.hpp"

namespace stream9::xdg {

using mime::mime_type;

} // namespace stream9::xdg


#endif // STREAM9_XDG_MIME_TYPE_HPP
