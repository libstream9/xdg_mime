#ifndef STREAM9_XDG_MIME_ENVIRONMENT_HPP
#define STREAM9_XDG_MIME_ENVIRONMENT_HPP

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

class environment
{
public:
    enum class file_type_t {
        regular,
        directory,
        socket,
        fifo,
        character_device,
        block_device,
    };

public:
    virtual ~environment() noexcept = default;

    // query
    virtual file_type_t file_type(cstring_ptr const& pathname);
    virtual bool is_symbolic_link(cstring_ptr const& pathname);
    virtual bool is_directory(cstring_ptr const& pathname);
    virtual bool is_regular_file(cstring_ptr const& pathname);
    virtual bool is_file_exists(cstring_ptr const& pathname);
    virtual bool is_executable(cstring_ptr const& pathname);
    virtual bool is_non_empty_directory(cstring_ptr const& pathname);
    virtual bool is_mount_point(cstring_ptr const& dir_path);

    virtual string normalize_path(cstring_ptr const& pathname);
    virtual string real_path(cstring_ptr const& pathname);
    virtual opt<string> real_path_icase(cstring_ptr const& pathname);

    virtual string load_file(cstring_ptr const& pathname);
    virtual void load_file(cstring_ptr const& pathname, array<char>& buffer);

    virtual bool match_filename(cstring_ptr const& pattern,
                                cstring_ptr const& filename,
                                bool case_insensitive = true);
};

environment& env();

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_ENVIRONMENT_HPP
