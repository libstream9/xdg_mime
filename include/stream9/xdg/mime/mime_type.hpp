#ifndef STREAM9_XDG_MIME_MIME_TYPE_HPP
#define STREAM9_XDG_MIME_MIME_TYPE_HPP

#include "error.hpp"
#include "namespace.hpp"

#include <compare>
#include <iosfwd>

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/node.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime {

class database;

class mime_type
{
public:
    mime_type(database const&, string_view name);

    mime_type(mime_type const&) = delete;
    mime_type& operator=(mime_type const&) = delete;

    mime_type(mime_type&&) noexcept;
    mime_type& operator=(mime_type&&) noexcept;

    ~mime_type() noexcept;

    // query
    cstring_view name() const noexcept;
    cstring_view comment() const;
    cstring_view comment(string_view language) const;
    array<cstring_view> aliases() const;
    array<mime_type> base_classes() const;
    bool is_subclass_of(string_view base) const noexcept;
    string icon_name() const;
    string generic_icon_name() const;
    cstring_view acronym() const;
    cstring_view acronym(string_view language) const;
    cstring_view expanded_acronym() const;
    cstring_view expanded_acronym(string_view language) const;
    array<cstring_view> glob_patterns() const;

    // operator
    auto operator<=>(mime_type const&) const;
    bool operator==(mime_type const&) const;

    auto operator<=>(string_view const& name) const;
    bool operator==(string_view const& name) const;

private:
    class impl;
    node<impl> m_p;
};

std::ostream& operator<<(std::ostream&, mime_type const&);

inline auto mime_type::
operator<=>(mime_type const& other) const
{
    return name() <=> other.name();
}

inline bool mime_type::
operator==(mime_type const& other) const
{
    return std::is_eq(*this <=> other);
}

inline auto mime_type::
operator<=>(string_view const& other) const
{
    return name() <=> other;
}

inline bool mime_type::
operator==(string_view const& other) const
{
    return std::is_eq(*this <=> other);
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_MIME_TYPE_HPP
