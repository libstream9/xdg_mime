#ifndef STREAM9_XDG_MIME_DATABASE_UPDATER_HPP
#define STREAM9_XDG_MIME_DATABASE_UPDATER_HPP

#include "database.hpp"

#include <stream9/linux/fd.hpp>
#include <stream9/node.hpp>

namespace stream9::xdg::mime {

class database_updater
{
public:
    // essential
    database_updater(database&);
    ~database_updater() noexcept;

    // query
    lx::fd_ref event_fd() const noexcept;

    // modifier
    void process_event();

private:
    struct impl;
    node<impl> m_p;
};

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_DATABASE_UPDATER_HPP
