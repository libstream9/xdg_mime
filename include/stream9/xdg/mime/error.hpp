#ifndef STREAM9_XDG_MIME_ERROR_HPP
#define STREAM9_XDG_MIME_ERROR_HPP

#include "namespace.hpp"

#include <source_location>
#include <system_error>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>

namespace stream9::xdg::mime {

enum class errc {
    ok = 0,
    parse_error,
    fail_to_load_file,
    cache_version_mismatch,
    no_root_element,
    file_does_not_exist,
    directory_does_not_exist,
    no_default_mime_type,
    unknown_file_type,
    fail_to_get_file_status,
    fail_to_get_canonical_path,
};

std::error_category const& error_category() noexcept;

inline std::error_code
make_error_code(errc e) noexcept
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::xdg::mime

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::mime::errc> : true_type {};

} // namespace std

#endif // STREAM9_XDG_MIME_ERROR_HPP
