#ifndef STREAM9_XDG_MIME_NAMESPACE_HPP
#define STREAM9_XDG_MIME_NAMESPACE_HPP

namespace stream9::strings {}
namespace stream9::filesystem {}
namespace stream9::iterators {}

namespace stream9::xdg::mime {

namespace fs { using namespace stream9::filesystem; }
namespace str { using namespace stream9::strings; }
namespace iter { using namespace stream9::iterators; }

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_NAMESPACE_HPP
