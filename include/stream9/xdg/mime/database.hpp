#ifndef STREAM9_XDG_MIME_DATABASE_HPP
#define STREAM9_XDG_MIME_DATABASE_HPP

#include "file_data.hpp"
#include "mime_type.hpp"
#include "namespace.hpp"

#include <cstdint>

#include <stream9/cstring_ptr.hpp>
#include <stream9/node.hpp>
#include <stream9/optional.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_array.hpp>

namespace stream9::xdg::mime {

class database_impl;

class database
{
public:
    using mime_type_set = unique_array<mime_type>;

public:
    database();
    ~database() noexcept;

    /**
     * Find mime type for name
     * If name is alias, it is unaliased before looking up mime type
     */
    opt<mime_type> find_mime_type_for_name(string_view name) const;

    /**
     * Lookup mime type for file
     *
     * First, lookup mime type with filename. If mime type can't be determined
     * from filename and givin path points to a readable file on filesystem
     * then conduct magic sniffing.
     *
     * @return default mime type if mime type can't be determined by a file
     */
    mime_type find_mime_type_for_file(cstring_ptr const& path) const;

    /**
     * Lookup mime type with filename and data
     *
     * First, lookup mime type with filename. If mime type can't be determined
     * from filename then conduct magic sniffing with given data.
     *
     * @return default mime type if mime type can't be determined by neither
     * filename nor data.
     */
    mime_type find_mime_type_for_file(string_view filename, file_data const&) const;

    /**
     * Lookup mime type by matching filename with glob
     *
     * If multiple mime types matches with filename, choose best match by
     * length of matched pattern and its weight.
     *
     * @return default mime type if mime type can't be determined by filename
     */
    mime_type find_mime_type_for_filename(string_view filename) const;

    /**
     * Lookup mime type by matching data with magic
     *
     * If multiple mime types matches with data, choose best match by
     * priority of magic.
     *
     * @return default mime type if mime type can't be determined by data
     */
    mime_type find_mime_type_for_data(file_data const&) const;

    /**
     * Lookup mime types by matching filename with glob
     *
     * @return every candidate that match.
     */
    mime_type_set find_mime_types_for_filename(string_view filename) const;

    /**
     * Lookup mime types by matching data with magic
     *
     * @return every candidate that match.
     */
    mime_type_set find_mime_types_for_data(file_data const&) const;

    /**
     * Lookup mime types of a volume
     *
     * @param root path to root directory of volume
     * @return every candidate that match.
     */
    mime_type_set find_mime_types_for_filesystem(cstring_ptr const& root_path) const;

    /**
     * Lookup mime type of XML document with its contents
     *
     * @param xml valid XML document
     */
    mime_type_set find_mime_type_for_xml(string_view xml) const;

    /**
     * Lookup mime type of XML document with its namespace and name of
     * root element.
     */
    mime_type_set find_mime_type_for_xml_namespace(
                        string_view namespace_uri,
                        string_view local_name) const;

    /**
     * Lookup mime type of non regular files
     *
     * Detect mime type for directory, symlink, block device, character device,
     * fifo, socket, mount point.
     */
    mime_type_set find_mime_type_for_non_regular_file(cstring_ptr const& path) const;

    /**
     * Get maximum size of data required to conduct magic sniffing.
     */
    std::uint32_t max_extent() const noexcept;

private:
    friend class mime_type;
    friend class database_updater;

    node<database_impl> m_p;
};

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_DATABASE_HPP
